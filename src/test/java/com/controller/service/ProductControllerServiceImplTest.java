package com.controller.service;

import com.database.repository.ProductRepository;
import com.dto.BaseTextResponse;
import com.dto.product.ProductDTO;
import com.service.ProductControllerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class ProductControllerServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductControllerServiceImpl productControllerService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllProducts() {
        List<ProductDTO> expectedProductList = new ArrayList<ProductDTO>() {{
            add(createProductDTO("Test Product 1", UUID.randomUUID()));
            add(createProductDTO("Test Product 2", UUID.randomUUID()));
        }};

        when(productRepository.getAllProducts())
                .thenReturn(expectedProductList);

        List<ProductDTO> actualProductList = productControllerService.getAllProducts();

        assertNotNull(actualProductList);
        assertEquals(2, actualProductList.size());
        assertEquals(expectedProductList, actualProductList);
    }

    @Test
    public void testGetProductById() {
        UUID id = UUID.randomUUID();
        ProductDTO expectedProduct = createProductDTO("Test Product", id);

        when(productRepository.getProductById(id))
                .thenReturn(expectedProduct);

        ProductDTO actualProduct = productControllerService.getProductById(id);

        assertNotNull(actualProduct);
        assertEquals(expectedProduct.getId(), actualProduct.getId());
    }

    @Test
    public void testSearchProducts() {
        List<ProductDTO> expectedProductList = new ArrayList<ProductDTO>() {{
            add(createProductDTO("Test Product 1", UUID.randomUUID()));
            add(createProductDTO("Test Product 2", UUID.randomUUID()));
        }};

        when(productRepository.searchProducts("Test", new BigDecimal("0.00"), new BigDecimal("100.00")))
                .thenReturn(expectedProductList);

        List<ProductDTO> actualProductList =
                productControllerService.searchProducts("Test", new BigDecimal("0.00"), new BigDecimal("100.00"));

        assertNotNull(actualProductList);
        assertEquals(2, actualProductList.size());
        assertEquals(expectedProductList, actualProductList);
    }

    @Test
    public void testAddProductSaveSuccessful() {
        ProductDTO productDTO = createProductDTO("Test Product", UUID.randomUUID());
        BaseTextResponse expectedResponse = new BaseTextResponse(
                String.format("Продукт успішно додано з id: %s", productDTO.getId()),
                BaseTextResponse.Status.SUCCESS
        );

        when(productRepository.addProduct(productDTO))
                .thenReturn(true);

        BaseTextResponse actualResponse = productControllerService.addProduct(productDTO);

        assertEquals(expectedResponse.getStatus(), actualResponse.getStatus());
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void testAddProductNotSuccessful() {
        ProductDTO productDTO = createProductDTO("Test Product", UUID.randomUUID());
        BaseTextResponse expectedResponse = new BaseTextResponse(
                "Не вдалося додати продукт",
                BaseTextResponse.Status.ERROR
        );

        when(productRepository.addProduct(productDTO))
                .thenReturn(false);

        BaseTextResponse actualResponse = productControllerService.addProduct(productDTO);

        assertEquals(expectedResponse.getStatus(), actualResponse.getStatus());
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void testUpdateProductById() {
        ProductDTO productDTO = createProductDTO("Test Product", UUID.randomUUID());
        BaseTextResponse expectedResponse = new BaseTextResponse(
                String.format("Продукт успішно оновлено з id: %s", productDTO.getId()),
                BaseTextResponse.Status.SUCCESS
        );

        when(productRepository.updateProductById(productDTO))
                .thenReturn(true);

        BaseTextResponse actualResponse = productControllerService.updateProductById(productDTO);

        assertEquals(expectedResponse.getStatus(), actualResponse.getStatus());
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void testDeleteProductByIdSuccessful() {
        UUID productId = UUID.randomUUID();
        BaseTextResponse expectedResponse = new BaseTextResponse(
                String.format("Продукт успішно видалено з id: %s", productId),
                BaseTextResponse.Status.SUCCESS
        );

        when(productRepository.deleteProductById(productId))
                .thenReturn(true);

        BaseTextResponse actualResponse = productControllerService.deleteProductById(productId);

        assertEquals(expectedResponse.getStatus(), actualResponse.getStatus());
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void testDeleteProductByIdNotSuccessful() {
        UUID productId = UUID.randomUUID();
        BaseTextResponse expectedResponse = new BaseTextResponse(
                String.format("Продукт з id: %s не знайдено", productId),
                BaseTextResponse.Status.ERROR
        );

        when(productRepository.deleteProductById(productId))
                .thenReturn(false);

        BaseTextResponse actualResponse = productControllerService.deleteProductById(productId);

        assertEquals(expectedResponse.getStatus(), actualResponse.getStatus());
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    private ProductDTO createProductDTO(String productName, UUID id) {
        return ProductDTO.builder()
                .id(id)
                .name(productName)
                .productDescription("Product Description")
                .price(new BigDecimal("100.00"))
                .quantityInStock(10L)
                .countryOfManufacture("Country")
                .build();
    }
}