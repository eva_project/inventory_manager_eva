package com.exception.handler;

import com.dto.BaseTextResponse;
import com.exception.BadRequestException;
import com.exception.FichaNotImplementedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlerService {

    @ExceptionHandler(FichaNotImplementedException.class)
    public ResponseEntity<BaseTextResponse> handleFichaNotImplementedException(FichaNotImplementedException ex) {
        BaseTextResponse errorResponse = new BaseTextResponse(ex.getMessage(), BaseTextResponse.Status.ERROR);

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_IMPLEMENTED);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<BaseTextResponse> handleBadRequestException(BadRequestException ex) {
        BaseTextResponse errorResponse = new BaseTextResponse(ex.getMessage(), BaseTextResponse.Status.ERROR);

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
