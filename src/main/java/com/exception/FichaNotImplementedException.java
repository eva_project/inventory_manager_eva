package com.exception;

public class FichaNotImplementedException extends RuntimeException {
    public FichaNotImplementedException() {
        super("Method not implemented");
    }
}
