package com.database.repository;

import com.database.mapper.ProductMapper;
import com.dto.product.ProductDTO;
import com.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Repository
public class ProductRepositoryImpl implements ProductRepository {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        String sql = "SELECT * FROM products";

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();

        return namedParameterJdbcTemplate.query(sql, mapSqlParameterSource, new ProductMapper());
    }

    @Override
    public ProductDTO getProductById(UUID id) {
        String sql = "SELECT * FROM products WHERE id = :id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        return namedParameterJdbcTemplate.queryForObject(sql, mapSqlParameterSource, new ProductMapper());
    }

    @Override
    public List<ProductDTO> searchProducts(String name, BigDecimal minPrice, BigDecimal maxPrice) {
        StringBuilder sql = new StringBuilder("SELECT * FROM products WHERE name LIKE ?");
        List<Object> params = new ArrayList<>();
        params.add("%" + name + "%");

        if (minPrice != null) {
            sql.append(" AND price >= ?");
            params.add(minPrice);
        }

        if (maxPrice != null) {
            sql.append(" AND price <= ?");
            params.add(maxPrice);
        }

        return jdbcTemplate.query(sql.toString(), params.toArray(), new ProductMapper());
    }

    @Override
    public boolean addProduct(ProductDTO productDTO) {
        String sql = "INSERT INTO products (name, product_description, country_of_manufacture, price, quantity_in_stock) " +
                "VALUES (:name, :productDescription, :countryOfManufacture, :price, :quantityInStock)";

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("name", productDTO.getName())
                .addValue("productDescription", productDTO.getProductDescription())
                .addValue("price", productDTO.getPrice())
                .addValue("quantityInStock", productDTO.getQuantityInStock())
                .addValue("countryOfManufacture", productDTO.getCountryOfManufacture());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource) > 0;
    }

    private void checkIfPresent(Optional<Object> object,
                                List<String> updateClauses,
                                MapSqlParameterSource parameters, String columnName) {
        object.ifPresent(obj -> {
            updateClauses.add(columnName + " = :" + columnName);
            parameters.addValue(columnName, obj);
        });
    }

    @Override
    public boolean updateProductById(ProductDTO productDTO) {
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        List<String> updateClauses = new ArrayList<>();

        checkIfPresent(Optional.ofNullable(productDTO.getName()), updateClauses, parameters, "name");
        checkIfPresent(Optional.ofNullable(productDTO.getProductDescription()), updateClauses, parameters, "product_description");
        checkIfPresent(Optional.ofNullable(productDTO.getCountryOfManufacture()), updateClauses, parameters, "country_of_manufacture");
        checkIfPresent(Optional.ofNullable(productDTO.getPrice()), updateClauses, parameters, "price");
        checkIfPresent(Optional.ofNullable(productDTO.getQuantityInStock()), updateClauses, parameters, "quantity_in_stock");

        if (updateClauses.isEmpty()) {
            throw new BadRequestException("Не указаны поля для обновления");
        }

        String sql = "UPDATE products SET " +
                String.join(", ", updateClauses) +
                " WHERE id = :id";
        parameters.addValue("id", productDTO.getId());

        int rowsAffected = namedParameterJdbcTemplate.update(sql, parameters);

        return rowsAffected > 0;
    }

    @Override
    public boolean deleteProductById(UUID id) {
        String sql = "DELETE FROM products WHERE id = :id";
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("id", id);

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource) > 0;
    }
}

