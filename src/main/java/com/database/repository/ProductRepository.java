package com.database.repository;

import com.dto.product.ProductDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface ProductRepository {
    List<ProductDTO> getAllProducts();

    ProductDTO getProductById(UUID id);

    List<ProductDTO> searchProducts(String name, BigDecimal minPrice, BigDecimal maxPrice);

    boolean addProduct(ProductDTO productDTO);

    boolean updateProductById(ProductDTO productDTO);

    boolean deleteProductById(UUID id);
}
