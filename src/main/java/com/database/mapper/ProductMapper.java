package com.database.mapper;


import com.dto.product.ProductDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class ProductMapper implements RowMapper<ProductDTO> {
    @Override
    public ProductDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ProductDTO.builder()
                .id(UUID.fromString(rs.getString("id")))
                .name(rs.getString("name"))
                .productDescription(rs.getString("product_description"))
                .countryOfManufacture(rs.getString("country_of_manufacture"))
                .price(rs.getBigDecimal("price"))
                .quantityInStock(rs.getLong("quantity_in_stock"))
                .build();
    }
}
