package com.database.entity;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.math.BigDecimal;
import java.util.UUID;

@EntityScan
@Data
public class ProductEntity {
    private UUID id;
    private String name;
    private String productDescription;
    private BigDecimal price;
    private Long quantityInStock;
    private String countryOfManufacture;

    public ProductEntity() {
    }

    public ProductEntity(ProductEntityBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.productDescription = builder.productDescription;
        this.price = builder.price;
        this.quantityInStock = builder.quantityInStock;
        this.countryOfManufacture = builder.countryOfManufacture;
    }

    public static ProductEntityBuilder builder() {
        return new ProductEntityBuilder();
    }

    public static class ProductEntityBuilder {
        private UUID id;
        private String name;
        private String productDescription;
        private BigDecimal price;
        private Long quantityInStock;
        private String countryOfManufacture;

        public ProductEntityBuilder id(UUID id) {
            this.id = id;

            return this;
        }

        public ProductEntityBuilder name(String name) {
            this.name = name;

            return this;
        }

        public ProductEntityBuilder productDescription(String productDescription) {
            this.productDescription = productDescription;

            return this;
        }

        public ProductEntityBuilder price(BigDecimal price) {
            this.price = price;

            return this;
        }

        public ProductEntityBuilder quantityInStock(Long quantityInStock) {
            this.quantityInStock = quantityInStock;

            return this;
        }

        public ProductEntityBuilder countryOfManufacture(String countryOfManufacture) {
            this.countryOfManufacture = countryOfManufacture;

            return this;
        }

        public ProductEntity build() {
            return new ProductEntity(this);
        }

    }
}
