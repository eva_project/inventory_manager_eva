package com.controller;

import com.service.ProductControllerService;
import com.dto.BaseTextResponse;
import com.dto.product.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {
    private final ProductControllerService productControllerService;

    @Autowired
    public ProductController(@Qualifier("productControllerServiceImpl") ProductControllerService productControllerService) {
        this.productControllerService = productControllerService;
    }

    @GetMapping("/allProducts")
    public List<ProductDTO> getAllProducts() {
        return productControllerService.getAllProducts();
    }


    @GetMapping("/product")
    public ProductDTO getProductById(@RequestParam("id") UUID id) {
        return productControllerService.getProductById(id);
    }


    @GetMapping("/searchProduct")
    public List<ProductDTO> searchProducts(@RequestParam(value = "name", required = true) String name,
                                           @RequestParam(value = "minPrice", required = false) BigDecimal minPrice,
                                           @RequestParam(value = "maxPrice", required = false) BigDecimal maxPrice) {

        return productControllerService.searchProducts(name, minPrice, maxPrice);
    }

    @PostMapping("/addProduct")
    public BaseTextResponse addProduct(@RequestBody ProductDTO productDTO) {
        return productControllerService.addProduct(productDTO);
    }

    @PutMapping("/updateProduct")
    public BaseTextResponse updateProductById(@RequestBody ProductDTO productDTO) {
        return productControllerService.updateProductById(productDTO);
    }

    @DeleteMapping("/deleteProduct")
    public BaseTextResponse deleteProductById(@RequestParam("id") UUID id) {
        return productControllerService.deleteProductById(id);
    }
}
