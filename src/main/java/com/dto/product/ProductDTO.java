package com.dto.product;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("productDescription")
    private String productDescription;

    @JsonProperty("price")
    private BigDecimal price;

    @JsonProperty("quantityInStock")
    private Long quantityInStock;

    @JsonProperty("countryOfManufacture")
    private String countryOfManufacture;
}
