package com.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BaseTextResponse {
    private String message;
    private Status status;

    public enum Status {
        SUCCESS,
        ERROR
    }
}
