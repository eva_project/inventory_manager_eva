package com.service;

import com.dto.BaseTextResponse;
import com.dto.product.ProductDTO;
import com.exception.FichaNotImplementedException;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * The ProductControllerService interface provides a contract for product-related operations.
 * Implementations of this interface should provide the actual logic for these operations.
 *
 * <p>Each method in this interface corresponds to a specific operation related to products,
 * such as retrieving all products, retrieving a product by its ID, searching for products,
 * adding a new product, updating an existing product, and deleting a product.</p>
 *
 * <p>By default, each method throws a FichaNotImplementedException. This is a placeholder
 * implementation that should be overridden by concrete implementations of this interface.</p>
 */
public interface ProductControllerService {

    /**
     * Retrieves all products.
     *
     * @return a list of all products
     * @throws FichaNotImplementedException if this method is not overridden
     */
    default List<ProductDTO> getAllProducts() {
        throw new FichaNotImplementedException();
    }

    /**
     * Retrieves a product by its ID.
     *
     * @param id the ID of the product to retrieve
     * @return the product with the given ID
     * @throws FichaNotImplementedException if this method is not overridden
     */
    default ProductDTO getProductById(UUID id) {
        throw new FichaNotImplementedException();
    }

    /**
     * Searches for products based on the provided name and price range.
     *
     * @param name     the name of the product to search for
     * @param minPrice the minimum price of the product to search for
     * @param maxPrice the maximum price of the product to search for
     * @return a list of products that match the search criteria
     * @throws FichaNotImplementedException if this method is not overridden
     */
    default List<ProductDTO> searchProducts(String name, BigDecimal minPrice, BigDecimal maxPrice) {
        throw new FichaNotImplementedException();
    }

    /**
     * Adds a new product.
     *
     * @param productDTO the product to add
     * @return a response indicating the result of the operation
     * @throws FichaNotImplementedException if this method is not overridden
     */
    default BaseTextResponse addProduct(ProductDTO productDTO) {
        throw new FichaNotImplementedException();
    }

    /**
     * Updates a product by its ID.
     *
     * @param productDTO the product to update
     * @return a response indicating the result of the operation
     * @throws FichaNotImplementedException if this method is not overridden
     */
    default BaseTextResponse updateProductById(ProductDTO productDTO) {
        throw new FichaNotImplementedException();
    }

    /**
     * Deletes a product by its ID.
     *
     * @param id the ID of the product to delete
     * @return a response indicating the result of the operation
     * @throws FichaNotImplementedException if this method is not overridden
     */
    default BaseTextResponse deleteProductById(UUID id) {
        throw new FichaNotImplementedException();
    }
}
