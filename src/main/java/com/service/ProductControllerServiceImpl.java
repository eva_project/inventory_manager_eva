package com.service;

import com.database.repository.ProductRepository;
import com.dto.BaseTextResponse;
import com.dto.product.ProductDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class ProductControllerServiceImpl implements ProductControllerService {
    private final ProductRepository productRepository;

    public ProductControllerServiceImpl(@Qualifier("productRepositoryImpl") ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductDTO> getAllProducts() {
        log.info("Getting all products");

        return productRepository.getAllProducts();
    }

    @Override
    public ProductDTO getProductById(UUID id) {
        log.info("Getting product by id: {}", id);

        return productRepository.getProductById(id);
    }

    @Override
    public List<ProductDTO> searchProducts(String name, BigDecimal minPrice, BigDecimal maxPrice) {
        log.info("Searching products by name: {}, minPrice: {}, maxPrice: {}", name, minPrice, maxPrice);

        return productRepository.searchProducts(name, minPrice, maxPrice);
    }

    @Override
    public BaseTextResponse addProduct(ProductDTO productDTO) {
        log.info("Adding product: {}", productDTO);

        boolean isAdded = productRepository.addProduct(productDTO);

        if (isAdded) {
            return new BaseTextResponse(
                    String.format("Продукт успішно додано з id: %s", productDTO.getId()),
                    BaseTextResponse.Status.SUCCESS
            );
        } else {
            return new BaseTextResponse(
                    "Не вдалося додати продукт",
                    BaseTextResponse.Status.ERROR
            );
        }
    }

    @Override
    public BaseTextResponse updateProductById(ProductDTO productDTO) {
        log.info("Updating product: {}", productDTO);

        boolean isUpdated = productRepository.updateProductById(productDTO);

        if (isUpdated) {
            return new BaseTextResponse(
                    String.format("Продукт успішно оновлено з id: %s", productDTO.getId()),
                    BaseTextResponse.Status.SUCCESS
            );
        } else {
            return new BaseTextResponse(
                    String.format("Продукт з id: %s не знайдено", productDTO.getId()),
                    BaseTextResponse.Status.ERROR
            );
        }
    }

    @Override
    public BaseTextResponse deleteProductById(UUID id) {
        log.info("Deleting product by id: {}", id);

        boolean isDeleted = productRepository.deleteProductById(id);

        if (isDeleted) {
            return new BaseTextResponse(
                    String.format("Продукт успішно видалено з id: %s", id),
                    BaseTextResponse.Status.SUCCESS
            );
        } else {
            return new BaseTextResponse(
                    String.format("Продукт з id: %s не знайдено", id),
                    BaseTextResponse.Status.ERROR
            );
        }
    }
}
