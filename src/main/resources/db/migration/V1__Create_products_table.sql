CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE public.products (
                                 id uuid DEFAULT uuid_generate_v4() NOT NULL,
                                 name text NOT NULL,
                                 product_description text NULL,
                                 country_of_manufacture text NOT NULL,
                                 price numeric NOT NULL,
                                 quantity_in_stock int8 NOT NULL,
                                 CONSTRAINT products_pkey PRIMARY KEY (id)
);