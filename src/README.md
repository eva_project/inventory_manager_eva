# Inventory Management System for EVA

## Опис

**Inventory Management System for EVA** - це веб-додаток для управління запасами продуктів у продуктовому магазині. Додаток призначений для автоматизації роботи працівників магазину з обліку та управління асортиментом на складі.

## Архітектура проекту

Проект використовує мікросервісну архитектуру, що включає в себе як бекенд, так і фронтенд, написаний на Java.

- порт бекенда - 8080
- порт фронтенда - 8081

## Технології

- Java 8
- Spring Boot 2.7.18
- JDBC Template (Data Access)
- PostgreSQL (Database)
- Flyway (Database Migration)
- Maven (Build Tool)
- Lombok (Java Library)
- Tomcat (Server)
- JUnit 5 (Testing)
- Mockito (Testing)

## Встановлення та запуск

### Вимоги

#### Встановлення JDK
1. Перейдіть на [офіційний сайт Oracle](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html) і завантажте JDK 8.
2. Запустіть завантажений файл і дотримуйтесь інструкцій установника.
3. Після встановлення додайте JDK до змінної середовища PATH.

#### Встановлення Maven
1. Перейдіть на [офіційний сайт Maven](https://maven.apache.org/download.cgi) і завантажте останню версію.
2. Розпакуйте завантажений архів у будь-яку директорію.
3. Додайте шлях до розпакованої директорії Maven у змінну середовища PATH.

#### Встановлення PostgreSQL
- Встановіть PostgreSQL (за замовчуванням порт - 5432).

### Запуск проекту

1. Клонуйте репозиторій на ваш локальний комп'ютер.
2. Перейдіть до кореневого каталогу проекту через командний рядок.
3. Відкрийте файл `application.properties` у директорії `src/main/resources`. Змініть параметри підключення до бази даних на свої.
4. Створіть базу даних у PostgreSQL з ім'ям `inventory_manager_eva`.
5. Проект містить скрипти для створення таблиць та заповнення їх даними. Під час запуску проекту Flyway автоматично виконає скрипти міграції.

### Для запуску проекту з командного рядка:
```bash
mvn spring-boot:run
```

### Упаковка проекту для Tomcat:
```bash
mvn clean package
```
## Використання проекту

### Порт
- Проект працює на порту - 8080

### Postman Collection
- Для тестування API ви можете використати надану Postman колекцію. Посилання на колекцію: [InventoryManagerEVA.postman_collection.json](src/main/resources/InventoryManagerEVA.postman_collection.json)