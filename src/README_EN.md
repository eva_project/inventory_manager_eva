# Inventory Management System for EVA

## Description

**Inventory Management System for EVA** is a web application for managing the inventory of products in a grocery store. The application is designed to automate the work of store employees in accounting and managing the assortment in the warehouse.

## Project architecture

The project uses a microservice architecture that includes both a backend and a frontend written in Java.

- backend port - 8080
- frontend port - 8081

## Technologies

- Java 8
- Spring Boot 2.7.18
- JDBC Template (Data Access)
- PostgreSQL (Database)
- Flyway (Database Migration)
- Maven (Build Tool)
- Lombok (Java Library)
- Tomcat (Server)
- JUnit 5 (Testing)
- Mockito (Testing)

## Installation and Launch

### Requirements

#### Installing JDK
1. Go to the [official Oracle website](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html) and download JDK 8.
2. Run the downloaded file and follow the installer instructions.
3. After installation, add the JDK to the PATH environment variable.

#### Installing Maven
1. Go to the [official Maven website](https://maven.apache.org/download.cgi) and download the latest version.
2. Unpack the downloaded archive to any directory.
3. Add the path to the unpacked Maven directory to the PATH environment variable.

#### Installing PostgreSQL
- Install PostgreSQL (default port - 5432).

### Launching the Project

1. Clone the repository to your local computer.
2. Navigate to the root directory of the project through the command line.
3. Open the `application.properties` file in the `src/main/resources` directory. Change the database connection parameters to your own.
4. Create a database in PostgreSQL named `inventory_manager_eva`.
5. The project contains scripts for creating tables and populating them with data. When the project is launched, Flyway will automatically execute the migration scripts.

### To run the project from the command line:
```bash
mvn spring-boot:run
```

### Packaging the project for Tomcat:
```bash
mvn clean package
```

## Using the project

### Port
- The project is running at the port - 8080

### Postman Collection
- To test the API, you can use the Postman collection. Collection submission: [InventoryManagerEVA.postman_collection.json](src/main/resources/InventoryManagerEVA.postman_collection.json)